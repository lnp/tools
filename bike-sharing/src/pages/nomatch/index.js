import React, { Component } from 'react';

class NoMatch extends Component {
    render() {
        return (
            <div style={{ textAlign: 'center', fontSize: '24px', color: 'red' }}>
                404......
            </div>
        );
    }
}

export default NoMatch;